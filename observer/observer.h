#ifndef __OBSERVER_H__
#define __OBSERVER_H__

#include <list>
#include <algorithm>

/* interface */
class IObserver;

class IObservable
{
public:
	virtual void attach(IObserver *observer) = 0;
	virtual void deattach(IObserver *observer) = 0;
	virtual void daattachAll() = 0;
	virtual void notify() const = 0;
	virtual int countObservers() const = 0;
};

class IObserver
{
public:
	virtual void update() = 0;
	virtual void attach(IObservable *subject) = 0;
	virtual void deattach() = 0;
};

/* realization */
class Observable : public IObservable
{
public:
	virtual ~Observable() = 0;

	void attach(IObserver *observer) override final
	{
		if (observer == nullptr)
			return;

		if (std::find(observers_.begin(), observers_.end(), observer) == observers_.end())
		{
			observers_.push_back(observer);
			observer->attach(this);
		}
	}

	void deattach(IObserver *observer) override final
	{
		if (observer == nullptr)
			return;

		if (std::find(observers_.begin(), observers_.end(), observer) != observers_.end())
		{
			observers_.remove(observer);
			observer->deattach();
		}
	}

	void daattachAll() override final
	{
		auto it = observers_.begin();

		while (it != observers_.end()) {
			(*it)->deattach();
			it = observers_.begin();
		}
	}

	void notify() const override 
	{
		for (auto observer : observers_) {
			observer->update();
		}
	}

	int countObservers() const override final
	{
		return observers_.size();
	}

protected:
	std::list<IObserver*> observers_;
};

Observable::~Observable() {};

class Observer : public IObserver
{
public:
	Observer(IObservable *subject = nullptr)
	{
		attach(subject);
	};

	void attach(IObservable *subject) override final
	{
		if (subject == nullptr)
			return;

		if (subject != subject_)
			deattach();

		subject_ = subject;
		subject_->attach(this);
	}

	void deattach() override final
	{
		if (subject_ == nullptr)
			return;

		subject_->deattach(this);
		subject_ = nullptr;
	}

	void update() {}

protected:
	IObservable *subject_;
};

#endif // __OBSERVER_H__
